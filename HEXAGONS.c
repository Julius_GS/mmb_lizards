#include <stdio.h>
#include <math.h>
extern int xfield,yfield,scale,species,rseed,border,asyn,thetime,graphics,spacetime,popdyn;
extern char **state,**newstate;
double Uniform();
// define the switch probs here for easy overview:
// notation: [g|b][0-6], meaning cell of colour [g|b] with [0-6] black
// neighbours has the following probability to change colour
// from the author:
// const float g6 = 0;
// const float g5 = 0.0096;
// const float g4 = 0.0086;
// const float g3 = 0.0271;
// const float g2 = 0.0822;
// const float g1 = 0.1638;
// const float g0 = 0.2726;
// const float b0 = 0;
// const float b1 = 0.0014;
// const float b2 = 0.0069;
// const float b3 = 0.0090;
// const float b4 = 0.0380;
// const float b5 = 0.1275;
// const float b6 = 0.3433;
// const float ratio = 0.43372;
const float ratio = 1;
const float a = 0.001275;
const float b = a*ratio;
const float g6 = a*pow(0,3);
const float g5 = a*pow(1,3);
const float g4 = a*pow(2,3);
const float g3 = a*pow(3,3);
const float g2 = a*pow(4,3);
const float g1 = a*pow(5,3);
const float g0 = a*pow(6,3);
const float b0 = b*pow(0,3);
const float b1 = b*pow(1,3);
const float b2 = b*pow(2,3);
const float b3 = b*pow(3,3);
const float b4 = b*pow(4,3);
const float b5 = b*pow(5,3);
const float b6 = b*pow(6,3);
InitConstants()
{
     xfield    =  100;	 /* horizontal size of the automaton */
     yfield    =  100;	 /* vertical size of the automaton */
     scale    =   6;    /* scale of drawing */
     species  =   2;    /* number of states; range 1-256 */
     rseed    =   1;    /* seed of the randomgenerator */
     border   =   2;    /* 0 = empty borders; 1 = torus; 2 = echoing borders */
     asyn     =   1;    /* 1 = asynchronous updating; 0 = synchronous */
     graphics =   2;    /* 0 = program runs silently */
    spacetime = 0;    /* length of space/time plot; 0 = no space time plot */
    popdyn = 0;         /* 1 = population dynamics plot; 0 = no pop.dyn. plot */
}

 InitSpecies()
 {
      int x,y;

      InitSpeciesRandom();
 }


// I want to write a function that takes in the number of hexagonal neighbours
// and outputs the sum of scales that are of a specific colour
// remember: y goes down, x goes right. so y is actually the same for all,
// while x changes
static int eve_nx[9] = { 0, 0, 1,-1, 1, 0, -1};
static int odd_nx[9] = { 0, -1, 0,-1, 1, -1, 0};
static int all_ny[9] = { 0,-1,-1, 0, 0, 1, 1};

int HexNeighbours(x,y) int x,y;
{
    // counts the number of black neighbours
    int sum = 0;
    int i;
    if (y % 2 == 1){
        for (i=1;i<=6;i++)
        sum += state[x+odd_nx[i]][y+all_ny[i]];
    } else {
        for (i=1;i<=6;i++)
        sum += state[x+eve_nx[i]][y+all_ny[i]];
    }
    return(sum);
}

void NextState(x,y) int x,y;
{
    // if (x == 48 &&  y == 73){
    //     printf("juchuu: ");
    //     printf("(%d, ",x);
    //     printf("%d)\n",y);
    //     printf("(%d, ", *state[x,y]);
    //     printf("%d)\n", HexNeighbours(x,y));
    // }
    float rand = Uniform();
    // printf("%f: rand\n",rand);
    rand = rand*20;
	int amount_black_nb;
	amount_black_nb = HexNeighbours(x,y); // amount black neighbours
    if (state[x][y] == 0) {//green scale
        // printf("%d: BaG\n",amount_black_nb);
        switch(amount_black_nb) {
            case 0:
                if (rand < g0) {
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 1:
                if (rand < g1) {
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 2:
                if (rand < g2) { //0.02
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 3:
                if (rand < g3) { //0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 4:
                if (rand < g4) { //0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 5:
                if (rand < g5) { //0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 6:
                if (rand < g6) { //0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
        }
    } else { //black scale
        // printf("%d: BaB\n",amount_black_nb);
        switch(amount_black_nb) {
            case 6: 
                if (rand < b6) {
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 5: 
                if (rand < b5) { // eigentlich 0.045
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 4: 
                if (rand < b4) { // eigentlich 0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 3: 
                if (rand < b3) { // eigentlich 0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 2: 
                if (rand < b2) { // eigentlich 0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 1: 
                if (rand < b1) { // eigentlich 0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
            case 0: 
                if (rand < b0) { // eigentlich 0
                    newstate[x][y] = 1-state[x][y];
                }
                break;
        }
    }
    // compute total cell color distribution
    if (x == 1 && y == 1){
        // schreib die definition von checkstate hier mit rein!
        int totalblack = 0;
        int totalgreen = 0;
        int i, xi, yi;
        float black[]={0,0,0,0,0,0,0};
        float green[]={0,0,0,0,0,0,0};
        for (xi=1;xi<=xfield;xi=xi+1) {
            for (yi=1;yi<=yfield;yi=yi+1) {
                int black_nb = HexNeighbours(xi,yi);
                // printf("%d\n",state[xi][yi]);
                if (state[xi][yi] == 0) {
                    totalgreen += 1;
                    green[black_nb] += 1;
                } else {
                    totalblack +=1;
                    black[black_nb] += 1;
                }
            }
        }
        // find pos. of maximum in black[] and green[]:
        int maxgreen = green[0];
        int maxblack = black[0];
        for (int index = 1;i<7;i++) {
            if (green[i] > maxgreen) maxgreen = green[i];
            if (black[i] > maxblack) maxblack = black[i];
        }
        for (int i=0;i<7;i++) {
            if (green[i] == maxgreen){
                printf("\\#BaG: %d",i);
                printf(" (%d\\%)",(int)ceil(green[i]/totalgreen*100));
                printf("\n");
            }
            if (black[i] == maxblack){
                // printf("%d: most common \\#BaB",i);
                printf("\\#GaB: %d",6-i);
                printf(" (%d\\%),",(int)ceil(black[i]/totalblack*100));
                printf("\n");
            }
        }

        // printf("Proportion of green cells with n black neighbours\n");
        // for (i=0;i<=6;i++) {
        //     printf("%f",green[i]/totalgreen);
        //     printf(" (%d)", i);
        //     if (i == 4) {
        //         printf(" <---");
        //     }
        //     printf("\n");
        // }
        // printf("Proportion of black cells with n black neighbours\n");
        // for (i=0;i<=6;i++) {
        //     printf("%f",black[i]/totalblack);
        //     printf(" (%d)", i);
        //     if (i == 3) {
        //         printf(" <---");
        //     }
        //     printf("\n");
        // }
    }
}


 Report()
 {
      DrawField();
	/* DrawSpaceTime(xfield/2);
	 DrawPopDyn(0,1,1000,xfield*yfield/10,1);*/
      if (thetime%10==0) EchoTime();

	 }

 main()
 {
     OneLayer();
 }
